"""
@summary: Splits a multi fasta file into smaller files.
@requirements: biopython installed

@usage: python cfasta_split.py -i example.fasta -n 1000
@description: The example will create a directory called example, and
	within the directory smaller files names <filename>_N.fa with a
	thousand fasta entries in each of the smaller files.

@author:charles.hefer@gmail.com
@website: www.heferlab.com
@version:0.2
"""
import sys, os
import argparse
from Bio import SeqIO

def __main__():
	"""parse the command line arguments"""
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--input", required=True, dest="input", type=str,
		help="The input fasta file")
	parser.add_argument("-n", "--entries", default=1000, dest="entries",
		help="The number of fasta entries within each of the output files", type=int)
	parser.add_argument("-o", "--output", default=None, dest="outdir", 
		help="The name of the output folder, defaults to the filename (with extention removed)")
	args = parser.parse_args()
	
	#validation of the input options
	if not os.path.exists(args.input):
		print("Invalid input fasta file: %s" % args.input)
		sys.exit(1)
	if args.entries < 1:
		print("Invalid number of entries: %s" % args.entries)
		sys.exit(1)
	
	if args.outdir:
		dir_name = args.outdir
	else:
		dir_name = os.path.splitext(args.input)[0].replace(".", "")
	
	#create the output directory
	try:
		os.mkdir(dir_name)
	except FileExistsError:
		print("Can not create the output directory: %s, already exists" % dir_name)
		sys.exit(1)
		
	#Some internal variables
	entries = []
	file_number = 1
	file_name = os.path.splitext(args.input)[0].replace(".", "")
	
	with open(args.input, "r") as handle:
		for entry in SeqIO.parse(handle, "fasta"):
			#test how many entries
			if len(entries) < args.entries:
				entries.append(entry)
				
			#if number of entries has been reached
			else:
				filepath = os.path.join(dir_name, "%s_%s.fasta" % (file_name, str(file_number)))
				outhandle = open(filepath, "w")
				
				file_number += 1
				
				SeqIO.write(entries, outhandle, "fasta")
				outhandle.close()
				
				entries = [entry]
		
		#write the remainder
		filepath = os.path.join(dir_name, "%s_%s.fasta" % (file_name, str(file_number)))
		outhandle = open(filepath, "w")
		
		SeqIO.write(entries, outhandle, "fasta")
		outhandle.close()

if __name__ == "__main__":
	__main__()
	
	
	
