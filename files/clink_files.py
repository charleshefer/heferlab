"""
@summary: Links a list of files from the source dir
to the target directory

@usage: python clink_files.py -f files.txt -s source -t target
@description: The example will link all the entries in files.txt
from the source directory to the target directory

@author:charles.hefer@gmail.com
@website: www.heferlab.com
@version:0.1
"""
import sys, os
import argparse

def __main__():
	"""parse the command line arguments"""
	parser = argparse.ArgumentParser()
	parser.add_argument("-f", "--files", required=True, dest="files", type=str,
		help="The input list of files to copy or link")
	parser.add_argument("-s", "--source_dir", required=True, dest="source",
		help="The location of the source files", type=str)
	parser.add_argument("-t", "--target_dir", required=True, dest="target", 
		help="The path where to copy or link the files")
	args = parser.parse_args()
	
	#validation of the input options
	if not os.path.exists(args.files):
		print("Invalid input file: %s" % args.files)
		sys.exit(1)
	
	
	if not os.path.exists(args.source):
		print("Source directory does not exist: %s" % args.source)
		sys.exit(1)

	if not os.path.exists(args.target):
		print("Target directory does not exist: %s" % args.target)
		sys.exit(1)



	#open list of files
	with open(args.files, "r") as handle:
		for entry in handle:
			
			entry = entry.rstrip()
			
			entry_path = os.path.join(args.source, entry)
			target_path = os.path.join(args.target, entry)
			
			#see if the file exists
			if not os.path.exists(entry_path):
				sys.exit(1)

			if os.path.exists(entry_path):
				
				os.symlink(entry_path, target_path)

			else:
				print("Error setting up the directory")
				print(entry_path, target_path)
				sys.exit(1)
			
if __name__ == "__main__":
	__main__()
	
	
	
