"""
@summary: For every file in the target directory, quickly build a qsub script.
@requirements: None

@usage: python cqsub_batch.py 
@author:charles.hefer@gmail.com
@version:0.1
"""
import sys, os
import argparse

def args_error(message):
	"""Prints the message and exits"""
	print(message)
	sys.exit()
		
def create_qsub_file(file_entry, directory, extension, destination, jobname, queuename, nodes, 
						processors, command):
	"""Create the qsub_file"""
	print(file_entry, directory, extension, destination, jobname, queuename, nodes, processors, command)
	#rewrite the directory to include the full path
	directory = os.path.join(os.getcwd(), directory) 
	file_entry_full = os.path.join(directory, file_entry)
	#rewrite the destination to include the full path
	destination = os.path.join(os.getcwd(), destination)
	print(destination)
	with open(os.path.join(destination, file_entry + ".qsub"), "w") as temp_file:
		temp_file.write("#!/bin/bash\n")
		temp_file.write("#PBS -N %s\n" % jobname)
		temp_file.write("#PBS -q %s\n" % queuename)
		temp_file.write("#PBS -l nodes=%s:ppn=%s\n" % (nodes, processors))
		temp_file.write("cd $PBS_O_WORKDIR\n\n")
		temp_file.write(command.replace("<FILENAME>", file_entry_full + extension))
		

def __main__():
	"""parse the command line arguments"""
	parser = argparse.ArgumentParser()
	parser.add_argument("-d", "--directory", required=True, dest="directory", type=str,
		help="The name of the directory where the target files reside")
	parser.add_argument("-e", "--extension", required=True, dest="extension", type=str,
		help="The file extensions to work with, will filter the target directory for files of this type")
	parser.add_argument("-o", "--output", required=True, dest="output",
		help="The output directory, or the location where the qsubs will be created")
	parser.add_argument("-j", "--jobname", required=True, dest="jobname", type=str,
		help="The name of the job (PBS -N)")
	parser.add_argument("-q", "--queuename", required=True, dest="queuename",
		help="The name of the queue (PBS -q)")
	parser.add_argument("-n", "--nodes", required=True, dest="nodes",
		help="The number of nodes to use (PBS -l nodes=?)")
	parser.add_argument("-p", "--processors", required=True, dest="processors",
		help="The number of processors per node (PBS -l nodes=1:ppn=?)")
	parser.add_argument("-c", "--command", required=True, dest="command",
		help="The command to run. Use <FILENAME> to specify the filename in the command")
	args = parser.parse_args()
	
	#make sure that the output directory exists
	if not os.path.exists(args.output):
		try:
			os.mkdir(args.output)
		except OSError:
			print("Unable to create the qsub directory (--output): %s" % args.output)
	#make sure the file extension was specified
	if not args.extension:
		args_error("No file extensions (-e) specified, not sure what you want")
	else: #gather the list of files to work with
		print(os.listdir(os.path.join(os.getcwd())))
		files = [f.replace(args.extension, "") 
					for f in os.listdir(os.path.join(os.getcwd(), args.directory)) 
					if f.endswith(args.extension)]

	if not args.jobname:
		args_error("No PBS jobname specified (--jobname)")
	if not args.queuename:
		args_error("No PBS queue specified (--queuename)")
	if not args.nodes:
		args_error("Number of nodes not specified (--nodes)")
	if not args.processors:
		args_error("Number of processors not specified (--processors)")
	if not args.command:
		args_error("No command specified, doing nothing (--command)")
	
	
	for file_entry in files:
		create_qsub_file(file_entry,
			 				args.directory,
							args.extension,
							args.output,
							args.jobname,
							args.queuename,
							args.nodes,
							args.processors,
							args.command)
	


if __name__ == "__main__":
	__main__()
	
	
	
